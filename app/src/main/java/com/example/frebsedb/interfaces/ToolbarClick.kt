package com.example.frebsedb.interfaces

import android.view.View

interface ToolbarClick {
    abstract fun onLeftBtnClick(view:View)
    abstract fun onRightBtnClick(view:View)
}