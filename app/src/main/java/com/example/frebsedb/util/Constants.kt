package com.example.frebsedb.util

import android.os.Environment

open class Constants {
    companion object {
        val All_PERMISSION = 101
        val CAMERA_PERMISSION = 102
        val STORAGE_PERMISSION = 103
        val REQUEST_IMAGE = 104
        val PDF_REQUEST = 105
        val CAPTURE_IMAGE_REQ = 106
        val CALL_PERMISSION = 107

        val root =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath
        val TYPE_ALERT = "Alert"
        val TYPE_ERROR = "Error"
        val TYPE_SUCCESS = "Success"


    }
}