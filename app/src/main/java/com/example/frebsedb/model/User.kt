package com.example.frebsedb.model

data class User(
    var name: String? = null,
    var email: String? = null,
    var username: String? = null,
    var id: String? = null,
    var password: String? = null
)