package com.example.frebsedb.view.componant.viewmodel

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.frebsedb.model.User
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.util.*

class SignUpViewModel(val context: Context) : ViewModel() {
    var email = MutableLiveData<String>("")
    var pass = MutableLiveData<String>("")
    var name = MutableLiveData<String>("")
    var uname = MutableLiveData<String>("")
    var mDatabase: DatabaseReference = FirebaseDatabase.getInstance().getReference().child("users")
    fun onLoginClick(view: View) {
        (context as SignUpActivity).finish()
    }
    fun onSignupClick(view: View) {
        val model = User()
        model.email = email.value
        model.name = name.value
        model.username = uname.value
        model.password = pass.value
        model.id = UUID.randomUUID().toString()
       mDatabase.child("${System.currentTimeMillis()}").setValue(model)
        (context as SignUpActivity).finish()
    }

    class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return SignUpViewModel(
                context
            ) as T
        }
    }
}