package com.example.frebsedb

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment


import java.lang.NullPointerException

abstract class BaseFragment : Fragment(),
    View.OnClickListener {

    var mActivity: Activity? = null

    protected abstract fun setBinding(
        inflater: LayoutInflater?,
        container: ViewGroup?
    ): ViewDataBinding?

    init {

    }

    protected abstract fun createActivityObject()
    protected abstract fun initializeObject()
    protected abstract fun setListeners()
    override fun onResume() {
        super.onResume()
        if (mActivity == null) throw NullPointerException("must create activity object")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // sessionManager =
        //    SessionManager.Companion.getInstance(activity)

        val binding = setBinding(inflater, container)
        val view = binding!!.root
        setHasOptionsMenu(true)
        createActivityObject()
        initializeObject()
        return view
    }

    /*fun isCameraPermissionGiven(context: Context): Boolean {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (context.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED || context.checkSelfPermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_DENIED || context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || context.checkSelfPermission(
                        Manifest.permission.RECORD_AUDIO
                    ) == PackageManager.PERMISSION_DENIED
                ) {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO*//*,
                            Manifest.permission.MANAGE_EXTERNAL_STORAGE*//*
                        ),
                        Constants.FRAGMENT_PERMISSION_CAMERA_VIDEO_REQUEST_CODE
                    )
                    return false
                } else {
                    return true

                }
            } else {
                return true
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            return false
        }
    }*/


    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    override fun onClick(view: View) {}
}