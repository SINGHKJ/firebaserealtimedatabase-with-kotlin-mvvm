package com.example.frebsedb.view.componant.viewmodel

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.frebsedb.view.componant.activty.MainActivity
import com.example.frebsedb.view.componant.activty.LoginActivity
import com.google.firebase.database.*

class LoginViewModel(val context: Context) : ViewModel() {
    var email = MutableLiveData<String>("")
    var pass = MutableLiveData<String>("")
    var mDatabase: DatabaseReference = FirebaseDatabase.getInstance().getReference().child("users")

        fun onLoginClick(view: View) {
            val query=mDatabase.orderByChild("email").equalTo(email.value)
            query.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (dataSnapshot.exists()) {
                        (context as LoginActivity).startActivity(Intent(context,
                            MainActivity::class.java))
                    }else{
                        Toast.makeText(context,"Invalide email or password",Toast.LENGTH_SHORT).show()
                    }

                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })
        }
    fun onRegisterClick(view: View) {
        (context as LoginActivity).startActivity(Intent(context,
            SignUpActivity::class.java))
    }


    class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return LoginViewModel(
                context
            ) as T
        }
    }
}