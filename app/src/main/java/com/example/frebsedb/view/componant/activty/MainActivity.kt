package com.example.frebsedb.view.componant.activty

import android.os.Bundle
import android.view.View
import com.example.frebsedb.BaseBindingActivity
import com.example.frebsedb.R


class MainActivity : BaseBindingActivity() {
    override fun setBinding() {
        setContentView(R.layout.activity_main)
    }

    override fun createActivityObject(savedInstanceState: Bundle?) {
        mActivity = this
    }

    override fun initializeObject() {

    }

    override fun setListeners() {

    }

    override fun onLeftBtnClick(view: View) {

    }

    override fun onRightBtnClick(view: View) {

    }
}