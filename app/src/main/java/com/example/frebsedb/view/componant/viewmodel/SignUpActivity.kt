package com.example.frebsedb.view.componant.viewmodel

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.frebsedb.BaseBindingActivity
import com.example.frebsedb.R
import com.example.frebsedb.databinding.ActivitySignUpBinding

class SignUpActivity : BaseBindingActivity() {
    private lateinit var binding: ActivitySignUpBinding
    private var viewModel: SignUpViewModel?=null

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
        binding.lifecycleOwner=this
        viewModel = ViewModelProvider(this, SignUpViewModel.Factory(this))
            .get(SignUpViewModel::class.java)
        binding.viewmodel = viewModel
    }

    override fun createActivityObject(savedInstanceState: Bundle?) {
       mActivity=this
    }

    override fun initializeObject() {

    }

    override fun setListeners() {

    }



    override fun onLeftBtnClick(view: View) {

    }

    override fun onRightBtnClick(view: View) {

    }
}