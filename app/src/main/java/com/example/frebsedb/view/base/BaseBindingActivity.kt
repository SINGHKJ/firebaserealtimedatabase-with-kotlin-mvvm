package com.example.frebsedb

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.frebsedb.util.Constants
import com.example.frebsedb.util.Env
import java.io.File
import com.example.frebsedb.interfaces.ToolbarClick
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

abstract class BaseBindingActivity : AppCompatActivity(), ToolbarClick,
    View.OnClickListener {
    var mActivity: AppCompatActivity? = null

    protected abstract fun setBinding()
    protected abstract fun createActivityObject(savedInstanceState: Bundle?)
    protected abstract fun initializeObject()
    protected abstract fun setListeners()

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createActivityObject(savedInstanceState)
        setBinding()
        initializeObject()
        setListeners()

    }

    /*  override fun setToolbarCustomTitle(titleKey: String?) {}*/
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    public override fun onResume() {
        super.onResume()
        try {
            if (mActivity != null)
                Env.currentActivity = mActivity
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun onPause() {
        super.onPause()
    }


    override fun onClick(view: View) {}
    /* override fun showUpIconVisibility(isVisible: Boolean) {
         val actionBar = this.supportActionBar
         actionBar!!.setDisplayHomeAsUpEnabled(isVisible)
     }*/

    fun isAllPermissionGiven(): Boolean {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
                    || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        Constants.All_PERMISSION
                    )
                    return false
                } else {
                    return true

                }
            } else {
                return true
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            return false
        }
    }


    fun isCameraPermissionGiven(): Boolean {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
                    || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        Constants.CAMERA_PERMISSION
                    )
                    return false
                } else {
                    return true

                }
            } else {
                return true
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            return false
        }
    }


    fun isStoragePermissionGiven(): Boolean {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
                    || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
                ) {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        Constants.STORAGE_PERMISSION
                    )
                    return false
                } else {
                    return true

                }
            } else {
                return true
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            return false
        }
    }

    fun isCallPermissionGiven(): Boolean {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED
                ) {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.CALL_PHONE
                        ),
                        Constants.CALL_PERMISSION
                    )
                    return false
                } else {
                    return true

                }
            } else {
                return true
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            return false
        }
    }



    /*fun showSettingDialog(dialog: Dialog?, context: Context?) {
        lateinit var binding: LayoutCommonDialogBinding
        dialog?.getWindow()?.setBackgroundDrawableResource(android.R.color.transparent)
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.layout_common_dialog,
            null,
            false
        )
        dialog?.setContentView(binding.root)
        dialog?.setCancelable(false)
        binding.tvtitle.text = "Permission denied!!"
        binding.tvmessage.text =
            getString(R.string.go_to_setting_msg)
        binding.tvOk.visibility = View.GONE
        binding.linearYesNo.visibility = View.VISIBLE
        binding.tvYes.text = "Setting"
        binding.tvNo.text = "Cancel"
        binding.tvYes.setTextColor(resources.getColor(R.color.red_light))
        binding.tvNo.setTextColor(resources.getColor(R.color.color_black))
        binding.tvYes.setOnClickListener {
            dialog?.dismiss()
            val intent = Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", packageName, null)
            )
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            dialog?.dismiss()
        }
        binding.tvNo.setOnClickListener {
            dialog?.dismiss()
        }
        if (dialog != null && !dialog!!.isShowing) {
            dialog?.show()
        }

    }*/
}